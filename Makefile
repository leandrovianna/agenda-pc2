ARQUIVOS=main.c calendar.c atividade.c janela_main.c janela_dia.c janela_criar_ativ.c notificacao.c
ARQUIVOS1_OBJ=main.o calendar.o atividade.o janela_main.o janela_dia.o janela_criar_ativ.o
ARQUIVOS2_OBJ=atividade.o notificacao.o
GTK_VERSION=3.0
FLAGS=`pkg-config --libs --cflags gtk+-$(GTK_VERSION)` `pkg-config --cflags --libs libnotify` 

build:
	mkdir -p bin
	gcc -g -std=c99 -c $(ARQUIVOS) $(FLAGS)
	gcc $(ARQUIVOS1_OBJ) -o bin/main $(FLAGS)
	gcc $(ARQUIVOS2_OBJ) -o bin/notificacao $(FLAGS)

run:
	./notificacao &
	./main
