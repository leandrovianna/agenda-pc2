#include "janela_criar_ativ.h"

/* Callbacks */
static gboolean delete_event(GtkWidget *window, GdkEvent *event, gpointer data) {
	gtk_widget_hide(window);
	return TRUE;
}

static void cancelar_clicked(GtkButton *button, gpointer user_data) {
	gtk_widget_hide(GTK_WIDGET(user_data));
}

static void salvar_clicked(GtkButton *button, gpointer user_data) {
	JanelaCriarAtividade* j = (JanelaCriarAtividade*) user_data;

	struct tm *inicio, *fim;
	time_t inicio_t, fim_t;

	inicio = copy_tm(j->day);
	fim = copy_tm(j->day);

	inicio->tm_hour = gtk_spin_button_get_value(GTK_SPIN_BUTTON(j->inicio_hora));
	inicio->tm_min = gtk_spin_button_get_value(GTK_SPIN_BUTTON(j->inicio_min));
	inicio->tm_sec = gtk_spin_button_get_value(GTK_SPIN_BUTTON(j->inicio_seg));

	fim->tm_hour = gtk_spin_button_get_value(GTK_SPIN_BUTTON(j->fim_hora));
	fim->tm_min = gtk_spin_button_get_value(GTK_SPIN_BUTTON(j->fim_min));
	fim->tm_sec = gtk_spin_button_get_value(GTK_SPIN_BUTTON(j->fim_seg));

	g_printf("inicio: %s\nfim: %s\n", 
			asctime(inicio), asctime(fim));

	inicio_t = mktime(inicio);
	fim_t = mktime(fim);

	if (inicio_t >= fim_t) {
		g_printf("inicio_t >= fim_t\n");
		return ;
	}

	const char* str = gtk_entry_get_text(GTK_ENTRY(j->desc));
	if (!strcmp(str, "")) {
		g_printf("str == \"\"\n");
		return ;
	}

	Atividade *a = novaAtividade(inicio_t, fim_t, str);
	salvarAtividade(a);

	free(a);
	free(inicio);
	free(fim);
	janela_criar_atividade_close(j);
}
/* ----------- */

JanelaCriarAtividade* novaJanelaCriarAtividade(GtkBuilder* builder) {
	JanelaCriarAtividade* j = (JanelaCriarAtividade*) malloc(sizeof(JanelaCriarAtividade));
	if (!j) exit(1);

	gtk_builder_add_from_file(builder, UI_FILE, NULL);

	j->window = gtk_builder_get_object (builder, "window");
	g_signal_connect (j->window, "delete_event", G_CALLBACK (delete_event), NULL);

	gtk_window_set_title(GTK_WINDOW(j->window), "Cadastrar atividade");

	j->inicio_hora = gtk_builder_get_object(builder, "inicio_h");
	j->inicio_min = gtk_builder_get_object(builder, "inicio_m");
	j->inicio_seg = gtk_builder_get_object(builder, "inicio_s");
	j->fim_hora = gtk_builder_get_object(builder, "fim_h");
	j->fim_min = gtk_builder_get_object(builder, "fim_m");
	j->fim_seg = gtk_builder_get_object(builder, "fim_s");

	GtkAdjustment *ad1 = gtk_adjustment_new(0, 0, 23, 1, 1, 0);
	GtkAdjustment *ad2 = gtk_adjustment_new(0, 0, 23, 1, 1, 0);
	GtkAdjustment *ad3 = gtk_adjustment_new(0, 0, 60, 1, 1, 0);
	GtkAdjustment *ad4 = gtk_adjustment_new(0, 0, 60, 1, 1, 0);
	GtkAdjustment *ad5 = gtk_adjustment_new(0, 0, 60, 1, 1, 0);
	GtkAdjustment *ad6 = gtk_adjustment_new(0, 0, 60, 1, 1, 0);
	gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(j->inicio_hora), ad1);
	gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(j->fim_hora), ad2);
	gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(j->inicio_min), ad3);
	gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(j->fim_min), ad4);
	gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(j->inicio_seg), ad5);
	gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(j->fim_seg), ad6);

	j->desc = gtk_builder_get_object(builder, "desc");
	j->cancelarBt = gtk_builder_get_object(builder, "cancelarButton");
	j->salvarBt = gtk_builder_get_object(builder, "salvarButton");

	g_signal_connect(j->cancelarBt, "clicked", G_CALLBACK(cancelar_clicked), j->window);
	g_signal_connect(j->salvarBt, "clicked", G_CALLBACK(salvar_clicked), j);
	return j;
}

void janela_criar_atividade_show(JanelaCriarAtividade *j) {
	gtk_window_present(GTK_WINDOW(j->window));

	gtk_spin_button_set_value(GTK_SPIN_BUTTON(j->inicio_hora), 0);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(j->inicio_min), 0);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(j->inicio_seg), 0);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(j->fim_hora), 0);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(j->fim_min), 0);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(j->fim_seg), 0);
	gtk_entry_set_text(GTK_ENTRY(j->desc), "");
}

void janela_criar_atividade_close(JanelaCriarAtividade *j) {
	gtk_widget_hide(GTK_WIDGET(j->window));
}

