#ifndef JANELA_CRIAR_ATIV_H
#define JANELA_CRIAR_ATIV_H

#include <gtk/gtk.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "atividade.h"
#include "calendar.h"

#define UI_FILE ("form.ui")

typedef struct {
	GObject *window;
	GObject *inicio_hora,
			*inicio_min,
			*inicio_seg;
	GObject *fim_hora,
			*fim_min,
			*fim_seg;
	GObject *desc;
	GObject *cancelarBt, *salvarBt;
	struct tm* day;
} JanelaCriarAtividade;

JanelaCriarAtividade* novaJanelaCriarAtividade(GtkBuilder* builder);
void janela_criar_atividade_show(JanelaCriarAtividade *j);
void janela_criar_atividade_close(JanelaCriarAtividade *j);

#endif
