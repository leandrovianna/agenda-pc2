#ifndef JANELA_DIA_H

#define JANELA_DIA_H

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>
#include "janela_criar_ativ.h"
#include "atividade.h"

typedef struct {
	GObject *window;
	GObject *addButton;
	struct tm *day;
	JanelaCriarAtividade *jca;
	GObject *list;
} JanelaDia;
		
JanelaDia* novaJanelaDia(GtkBuilder* builder);
void janela_dia_show(JanelaDia *j);
void janela_dia_set_dia(JanelaDia *j, struct tm *dia);

#endif //JANELA_DIA_H
