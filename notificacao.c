#include <gtk/gtk.h>
#include <libnotify/notify.h>
#include <sys/types.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "atividade.h"	

#define   LOCK_SH   1    
#define   LOCK_EX   2    
#define   LOCK_NB   4    
#define   LOCK_UN   8

int calcula_diferenca_dias(Atividade *evento){//FUNÇÃO PARA CALCULAR A DIFERENCA EM SEGUNDOS DE DOIS HORÁRIOS...
	time_t t = time(0);
	data_de_hoje = *localtime(&t);
	time_t aux = evento->inicio-t;
	
	if(aux<0)//SE O EVENTO JÁ PASSOU ENTÃO RETORNA 0 PARA NÃO SER NOTIFICADO.
		return 0;

	diferenca = *localtime(&aux);
//	printf("dif:%ld\n",aux);

	return aux;
}

int main () {
	notify_init("NOTIFICACOES");
	printf("---------notification engine developed by Rafael Castro.!----------\n");
	while(TRUE){
		Atividade *data;
		int tam;
		printf("Running...\n");
		data = lerAtividades(&tam);
		
		if(data!=NULL){
			frequencia = lerFrequencias(&tam_f);
			
			if(frequencia == NULL)//SE O ARQUIVO ESTIVER VAZIO ENTÃO ELE CRIA NOVAS FREQUENCIAS INICIALIZADAS EM ZERO.
				frequencia = nova_frequencia(tam,&tam_f);

			if(frequencia!=NULL){
				if(tam>tam_f){//SE A QUANTIDADE DE EVENTOS FOR MAIOR DO QUE A QUANTIDADE DE FREQUENCIAS NO ARQUIVO...
					nova_frequencia(tam,&tam_f);
					adicionar_frequencias(tam-tam_f, tam_f);
				}
				
				for(int j = 0 ; j < tam ; j++){//verifica eventos próximos...
					
					if(tempo=calcula_diferenca_dias(data+j)){
						if(frequencia[j]!=data_de_hoje.tm_mday){//VERIFICA SE JÁ FOI NOTIFICADO HOJE...
							notify_init("NOTIFICACOES");
							frequencia[j] = data_de_hoje.tm_mday;
							
							if(tempo/(24*60*60)>1)//COLOCANDO O TEXTO NO FORMATO ADEQUADO A SER EXIBIDO....
								sprintf(text,"Faltam apenas %d dias para o evento : %s!", tempo/(24*60*60), (data+j)->desc);
							else if(tempo/(24*60*60)==1)
								sprintf(text,"Faltam apenas 1 dia para o evento : %s!",(data+j)->desc);
							else if(tempo/(60*60)!=0||(tempo-60*(tempo/(60*60)))/60!=0)
								sprintf(text,"Faltam %d horas e %d minutos para o evento: %s", tempo/(60*60), (tempo-3600*(tempo/(60*60)))/60, (data+j)->desc);
							else
								sprintf(text,"O evento %s está prestes a começar..!",(data+j)->desc);

							aviso = notify_notification_new("Não se esqueça!",text,"/home/recartes/Imagens/vaidarnao.png");
							notify_notification_set_app_name(aviso,"Lembrete");
							notify_notification_show(aviso,NULL);
							g_object_unref(G_OBJECT(aviso));
							notify_uninit();
							atualizar_frequencias(tam);
						}
					}
				}
			}
		}
		//verifica se hoje é uma data comemorativa.
		time_t t = time(0);
		data_de_hoje = *localtime(&t);
		date = ler_datas_comemorativas(&tam);
		
		if(date!=NULL){
			date_frequencia = ler_comemorative_date_frequencia(&tam_f);

			if(date_frequencia == NULL)//se o arquivo estiver vazio então vai criar frequências inicializadas em Zero!
				date_frequencia = nova_frequencia_date(tam,&tam_f);

			if(date_frequencia!=NULL){
				if(tam>tam_f){
					nova_frequencia_date(tam,&tam_f);
					adicionar_frequencias_date(tam-tam_f,tam_f);
				}

				for(int j = 0 ; j < tam ; j++){
					if(date[j].dia == data_de_hoje.tm_mday&&date[j].mes == data_de_hoje.tm_mon+1&&date_frequencia[j]!=data_de_hoje.tm_year){
						notify_init("Data Comemorativa");
						date_frequencia[j] = data_de_hoje.tm_year;
						sprintf(dir,"/home/recartes/%s",date[j].img);
						aviso = notify_notification_new(date[j].nome,date[j].hist,dir);
						notify_notification_show(aviso,NULL);
						notify_uninit();
						atualizar_frequencias_date(tam);
					}
					if((data_de_hoje.tm_year+1900)%19+1==data_de_hoje.tm_mday){//páscoa é um feriado sem data fixa, e o dia pode ser obtido através do "número de ouro" (data_de_hoje.tm_year+1900)%19+1....
						notify_init("Data Comemorativa");
						date_frequencia[j] = data_de_hoje.tm_year;
						aviso = notify_notification_new("Hoje é domingo de Pascoa!","Desejamos a você uma feliz Páscoa!","/home/recartes/pascoa.png");
						notify_notification_show(aviso,NULL);
						notify_uninit();
						atualizar_frequencias_date(tam);
					}
				}
			}
		}

		sleep(10);
	}
	return 0;
}
