#include "atividade.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/file.h>
#include <libnotify/notify.h>

/* Cria novo objeto Atividade */
Atividade* novaAtividade(time_t inicio, time_t fim, const char* desc) {
	Atividade* a;
	a = (Atividade*) malloc(sizeof(Atividade));
	if (!a)
		exit(1);

	a->inicio = inicio;
	a->fim = fim;
	strcpy(a->desc, desc);
	return a;
}

/* Salva atividade em um arquivo */
void salvarAtividade(Atividade* atividade) {
	// tentando abrir o arquivo
	FILE *file = fopen(ATIVIDADE_FILE, "a");
	int result = flock(fileno(file), LOCK_UN);

	if (result) 
		return;

	// travando o arquivo
	result = flock(fileno(file), LOCK_EX);
	if (result)
		return ;

	fseek(file, 0, SEEK_END);	
	fwrite(atividade, sizeof(Atividade), 1, file);
	fclose(file);

	// destravando o arquivo
	flock(fileno(file), LOCK_UN);

	//ENVIA NOTIFICAÇÃO QUE O EVENTO FOI SALVO COM SUCESSO...
	notify_init("NOTIFICACOES");
	salvo = notify_notification_new(atividade->desc,"Evento salvo com sucesso!","/home/recartes/Imagens/floppy-128.png");
	notify_notification_set_app_name(salvo,"Lembrete");
	notify_notification_show(salvo,NULL);
	g_object_unref(G_OBJECT(salvo));
	notify_uninit();
}


/* Retorna todas as atividades de um arquivo */
Atividade* lerAtividades(int *tam) {
	// tentando abrir o arquivo
	FILE *file = fopen(ATIVIDADE_FILE, "r");

	if(file==NULL)
		return NULL;

	int result = flock(fileno(file), LOCK_UN);

	*tam = 0;
	
	if (result) 
		return NULL;

	// travando o arquivo
	result = flock(fileno(file), LOCK_EX);
	if (result)
		return NULL;

	fseek(file, 0, SEEK_END);
	int n = ftell(file) / sizeof(Atividade);

	Atividade* atividades = (Atividade*) malloc(n * sizeof(Atividade));

	fseek(file, 0, SEEK_SET);	
	fread(atividades, sizeof(Atividade), n, file);
	fclose(file);

	// destravando o arquivo
	flock(fileno(file), LOCK_UN);
	*tam = n;
	return atividades;
}

int* lerFrequencias(int *tam_f) {

	FILE *file = fopen(ATIVIDADE_FREQUENCIA, "r");

	if(file==NULL)
		return NULL;

	fseek(file, 0, SEEK_END);
	int n = ftell(file) / sizeof(int);
	
	int *freq = (int*) malloc(n * sizeof(int));

	fseek(file, 0, SEEK_SET);	
	fread(freq, sizeof(int), n, file);
	fclose(file);

	*tam_f = n;

	return freq;
}

int* nova_frequencia(int n,int *posicao) {
	int* a;
	a = (int*) realloc(frequencia, n*sizeof(int));
	if (!a){
		return NULL;
	}

	for(int i = 0 ; i+*posicao < n; i++)
		a[i+*posicao] = 0;

	FILE *file = fopen(ATIVIDADE_FREQUENCIA, "w");
	
	fwrite(a,sizeof(int),n,file);

	*posicao = n;

	fclose(file);

	return a;
}

void adicionar_frequencias(int tam,int posicao){
	FILE *file = fopen(ATIVIDADE_FREQUENCIA,"a");

	fseek(file, 0, SEEK_END);

	fwrite(frequencia+posicao, sizeof(int), tam, file);

	fclose(file);
}

void atualizar_frequencias(int tam){
	FILE *file = fopen(ATIVIDADE_FREQUENCIA,"w");

	fwrite(frequencia, sizeof(int), tam, file);

	fclose(file);
}

void adicionar_data_comemorativa(comemorative_date* cm_date) {
	// tentando abrir o arquivo
	FILE *file = fopen(COMEMORATIVE_DATES_FILE, "a");
	int result = flock(fileno(file), LOCK_UN);

	fseek(file, 0, SEEK_END);	
	fwrite(cm_date, sizeof(comemorative_date), 1, file);
	fclose(file);

	// destravando o arquivo
	flock(fileno(file), LOCK_UN);

}

comemorative_date* nova_data_comemorativa(int dia, int mes, char* imagem, char* historia, char *name) {
	comemorative_date *a;
	a = (comemorative_date*) malloc(sizeof(comemorative_date));
	if (!a)
		exit(1);

	a->dia = dia;
	a->mes = mes;
	strcpy(a->img, imagem);
	strcpy(a->hist, historia);
	strcpy(a->nome, name);
	return a;
}

comemorative_date* ler_datas_comemorativas(int *tam_c) {

	FILE *file = fopen(COMEMORATIVE_DATES_FILE, "r");

	if(file==NULL)
		return NULL;

	fseek(file, 0, SEEK_END);
	int n = ftell(file) / sizeof(comemorative_date);
	
	comemorative_date *freq = (comemorative_date*) malloc(n * sizeof(comemorative_date));

	fseek(file, 0, SEEK_SET);	
	fread(freq, sizeof(comemorative_date), n, file);
	fclose(file);

	*tam_c = n;

	return freq;
}

int* ler_comemorative_date_frequencia(int *tam_f) {

	FILE *file = fopen(COMEMORATIVE_DATES_FILE_FREQUENCIA, "r");

	if(file==NULL)
		return NULL;

	fseek(file, 0, SEEK_END);
	int n = ftell(file) / sizeof(int);
	
	int *freq = (int*) malloc(n * sizeof(int));

	fseek(file, 0, SEEK_SET);	
	fread(freq, sizeof(int), n, file);
	fclose(file);

	*tam_f = n;

	return freq;
}

int* nova_frequencia_date(int n,int *posicao) {
	int* a;
	a = (int*) realloc(date_frequencia, n*sizeof(int));

	if (!a){
		return NULL;
	}

	for(int i = 0 ; i+*posicao < n; i++)
		a[i+*posicao] = 0;

	FILE *file = fopen(COMEMORATIVE_DATES_FILE_FREQUENCIA, "w");
	
	fwrite(a,sizeof(comemorative_date),n,file);

	*posicao = n;

	fclose(file);

	return a;
}

void adicionar_frequencias_date(int tam,int posicao){
	FILE *file = fopen(COMEMORATIVE_DATES_FILE_FREQUENCIA,"a");

	fseek(file, 0, SEEK_END);

	fwrite(date_frequencia+posicao, sizeof(comemorative_date), tam, file);

	fclose(file);
}

void atualizar_frequencias_date(int tam){
	FILE *file = fopen(COMEMORATIVE_DATES_FILE_FREQUENCIA,"w");

	fwrite(date_frequencia, sizeof(int), tam, file);

	fclose(file);
}

