#include "janela_main.h"
#include <stdlib.h>

#define UI_FILE_MAIN ("screen.ui")

JanelaMain* novaJanelaMain(GtkBuilder* builder) {
	JanelaMain* janela = (JanelaMain*) malloc(sizeof(JanelaMain));
	//TODO: adicionar icone

	if (!janela) exit(1);
	
	gtk_builder_add_from_file(builder, UI_FILE_MAIN, NULL);

	janela->window = gtk_builder_get_object (builder, "window");
	gtk_window_set_title(GTK_WINDOW(janela->window), "Agenda");
	g_signal_connect (janela->window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

	janela->grid = gtk_builder_get_object (builder, "calendar");

	janela->spinButton = gtk_builder_get_object(builder, "yearmenu");

	janela->comboBox = gtk_builder_get_object(builder, "monthmenu");

	janela->calendar = novoCalendarioHoje();
	inserir_meses_combo_box(GTK_COMBO_BOX_TEXT(janela->comboBox));
	gtk_combo_box_set_active(GTK_COMBO_BOX(janela->comboBox), janela->calendar->mes);

	GtkAdjustment* adjustment = gtk_adjustment_new(janela->calendar->ano, 1900, 4000, 1, 2, 0);
	gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(janela->spinButton), adjustment);

	return janela;
}
