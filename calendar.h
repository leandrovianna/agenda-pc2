#ifndef CALENDAR_H   /* Include guard */
#define CALENDAR_H

#define CALENDAR_ROWS 7
#define CALENDAR_COLUMS 7
#define DAY_FORMAT ("<span font='60'>%s</span>")
#define TODAY_FORMAT ("<span color='#ff0000' font='60'>%s</span>")
#define INACTIVE_DAY_FORMAT ("<span color='#888888' font='60'>%s</span>")
#define PADDING 20

#include <gtk/gtk.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
	int mes; //[0-11]
	int ano; //[1900-INF]
	void (*dayButtonCallback)(GtkButton*, gpointer);
} Calendario;

int get_weekday(int dia, int mes, int ano);
void calendar_update_grid(Calendario *calendar, GtkGrid *grid);
Calendario* novoCalendario(int mes, int ano);
Calendario* novoCalendarioHoje();
GtkComboBoxText* inserir_meses_combo_box(GtkComboBoxText*);
struct tm* convert_date_to_tm(int day, int mon, int year);
int isLeapYear(int year);
struct tm* copy_tm(struct tm* t);

#endif // CALENDAR_H
