#include <gtk/gtk.h>
#include "janela_main.h"
#include "janela_dia.h"
#include "calendar.h"
#include "janela_criar_ativ.h"

void dayClicked(GtkButton* button, gpointer user_data);
void year_changed(GtkSpinButton* spinButton, gpointer user_data);
void month_changed(GtkComboBoxText* comboBox, gpointer user_data);

JanelaMain* janelaMain;
JanelaDia* janelaDia;

int main(int argc, char* argv[]) {
	GtkBuilder* builder;

	gtk_init (&argc, &argv);

	builder = gtk_builder_new();

	janelaMain = novaJanelaMain(builder);
	g_signal_connect(janelaMain->spinButton, "value-changed", G_CALLBACK(year_changed), NULL);
	g_signal_connect(janelaMain->comboBox, "changed", G_CALLBACK(month_changed), NULL);

	janelaMain->calendar->dayButtonCallback = dayClicked;
	calendar_update_grid(janelaMain->calendar, GTK_GRID(janelaMain->grid));

	janelaDia = novaJanelaDia(builder);
	gtk_widget_set_visible(GTK_WIDGET(janelaDia->window), FALSE);

	gtk_main ();

	g_free(janelaDia);
	g_free(janelaMain);
	return 0;
}

void dayClicked(GtkButton* button, gpointer user_data) {
	janela_dia_set_dia(janelaDia, convert_date_to_tm(GPOINTER_TO_INT(user_data), 
			janelaMain->calendar->mes, janelaMain->calendar->ano));
	janela_dia_show(janelaDia);
}

void year_changed(GtkSpinButton* spinButton, gpointer user_data) {
	gint year = gtk_spin_button_get_value_as_int(spinButton);

	janelaMain->calendar->ano = year;
	calendar_update_grid(janelaMain->calendar, GTK_GRID(janelaMain->grid));
}

void month_changed(GtkComboBoxText* comboBox, gpointer user_data) {
	gint id = gtk_combo_box_get_active(GTK_COMBO_BOX(comboBox));

	if (id != -1) {
		janelaMain->calendar->mes = id;
		calendar_update_grid(janelaMain->calendar, GTK_GRID(janelaMain->grid));
	}
}

