#ifndef JANELA_MAIN_H

#define JANELA_MAIN_H

#include <gtk/gtk.h>

#include "calendar.h"

typedef struct {
	GObject *window;
	GObject *grid;
	GObject *spinButton;
	GObject *comboBox;
	Calendario *calendar;
	GCallback* yearChanged;
	GCallback* monthChanged;
} JanelaMain;
		
JanelaMain* novaJanelaMain(GtkBuilder* builder);

#endif // JANELA_MAIN_H
