#include "janela_dia.h"

#define UI_FILE_JANELA ("dia.ui")

static gboolean delete_event(GtkWidget *window, GdkEvent *event, gpointer data) {
	JanelaDia *j = (JanelaDia*) data;
	janela_criar_atividade_close(j->jca);
	gtk_widget_hide(window);
	return TRUE;
}

static void addButton_clicked(GtkButton* bt, gpointer user_data) {
	JanelaDia *j = (JanelaDia*) user_data;
	janela_criar_atividade_show(j->jca);
}

static gint sort(GtkListBoxRow *row1, GtkListBoxRow *row2, gpointer user_data) {
	gchar* str1 = gtk_label_get_text(GTK_LABEL(gtk_bin_get_child(GTK_BIN(row1))));
	gchar* str2 = gtk_label_get_text(GTK_LABEL(gtk_bin_get_child(GTK_BIN(row2))));

	return strcmp(str1, str2);
}

JanelaDia* novaJanelaDia(GtkBuilder* builder) {
	JanelaDia* j = (JanelaDia*) malloc(sizeof(JanelaDia));
	if (!j) exit(1);

	gtk_builder_add_from_file(builder, UI_FILE_JANELA, NULL);

	j->window = gtk_builder_get_object (builder, "window");
	g_signal_connect (j->window, "delete_event", G_CALLBACK (delete_event), j);

	j->addButton = gtk_builder_get_object(builder, "addButton");
	g_signal_connect(j->addButton, "clicked", G_CALLBACK(addButton_clicked), j);

	j->jca = novaJanelaCriarAtividade(builder);

	j->list = gtk_builder_get_object(builder, "list");
	gtk_list_box_set_sort_func(GTK_LIST_BOX(j->list), sort, NULL, NULL);

	return j;
}

void janela_dia_set_dia(JanelaDia *j, struct tm *dia) {
	j->jca->day = copy_tm(dia);
}

void janela_dia_show(JanelaDia *j) {
	gchar *str = g_strdup_printf("Dia %02d/%02d/%02d", 
			j->jca->day->tm_mday,
			j->jca->day->tm_mon + 1,
			j->jca->day->tm_year + 1900);

	gtk_window_set_title(GTK_WINDOW(j->window), str);

	g_free(str);

	gtk_container_forall(GTK_CONTAINER(j->list), gtk_widget_destroy, NULL);

	int n, i;
	Atividade *atividades = lerAtividades(&n);
	
	if (atividades != NULL) {
		struct tm *inicio, *fim;
		GtkWidget *label;

		for (i = 0; i < n; i++) {
			Atividade *a = atividades+i;
			inicio = copy_tm(localtime(&a->inicio));
			fim = copy_tm(localtime(&a->fim));

			if (inicio->tm_mday == j->jca->day->tm_mday
					&& inicio->tm_mon == j->jca->day->tm_mon
					&& inicio->tm_year == j->jca->day->tm_year) {
				str = g_strdup_printf("%02d:%02d:%02d - %02d:%02d:%02d\n%s",
						inicio->tm_hour, inicio->tm_min, inicio->tm_sec,
						fim->tm_hour, fim->tm_min, fim->tm_sec,
						a->desc);

				label = gtk_label_new(str);
				gtk_list_box_prepend(GTK_LIST_BOX(j->list), label);
				g_free(str);
				free(inicio);
				free(fim);
			}
		}

		free(atividades);
	}

	gtk_widget_show_all(GTK_WIDGET(j->list));
	gtk_window_present(GTK_WINDOW(j->window)); // foca a janela e a deixa visivel
}
