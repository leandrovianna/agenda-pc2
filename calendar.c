#include "calendar.h"

/* Retorna 1 se o ano passado for bissexto 
 * 0, caso contrário */
int isLeapYear(int year) {
	if (year % 400 == 0)
		return 1;
	else if (year % 4 == 0 && year % 100 != 0)
		return 1;
	else
		return 0;
}

/* Retorna o dia da semana de um dia qualquer
 * dia [1 - 31]
 * mes [0 - 11]
 * ano [0 - INF]
 * 0 - Domingo, ..., 6 - Sábado 
 * https://en.wikipedia.org/wiki/Zeller%27s_congruence */
int get_weekday(int dia, int mes, int ano) {
	mes++;

	// valor de mes para o algoritmo
	// Janeiro e Fevereiro sao 13 e 14 do ano anterior
	if (mes == 1 || mes == 2) {
		mes += 12;
		ano--;
	}

	int h = (dia + 13 * (mes + 1) / 5 + ano + ano / 4 - ano / 100 + ano / 400) % 7;
	return (h + 6) % 7; //normalizando para o intervalo descrito acima
}

static GtkWidget* create_day_box(Calendario *calendar, gint day, const gchar* format, gboolean inactive) {
	GtkWidget *box, *label, *button;
	gchar *str, *markup;

	box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	label = gtk_label_new(NULL);

	if (inactive) {
		gtk_box_pack_start(GTK_BOX(box), label, TRUE, TRUE, PADDING);
	} else {
		button = gtk_button_new();
		g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(calendar->dayButtonCallback), GINT_TO_POINTER(day));
		gtk_container_add(GTK_CONTAINER(button), label);
		gtk_box_pack_start(GTK_BOX(box), button, TRUE, TRUE, PADDING);
	}

	str = g_malloc(3 * sizeof(gchar));
	g_sprintf(str, "%02d", day);
	markup = g_markup_printf_escaped(format, str);
	gtk_label_set_markup(GTK_LABEL(label), markup);

	g_free(markup);
	g_free(str);
	return box;
}

void calendar_update_grid(Calendario *calendar, GtkGrid *grid) {
	int weekFirstDay, qtdDays, i, j, todayMDay = 0, k;
	struct tm *day;
	GtkWidget *box, *child;

	int CAL_MONTH_DAYS[] = {31, 28+isLeapYear(calendar->ano),
		31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	int previous_mon = calendar->mes - 1;
	if (previous_mon < 0) previous_mon = 11;

	weekFirstDay = get_weekday(1, calendar->mes, calendar->ano);

	time_t todayTimer = time(0);
	day = localtime(&todayTimer);
	if (day->tm_mon == calendar->mes && day->tm_year + 1900 == calendar->ano)
		todayMDay = day->tm_mday;

	for (i = 1; i < CALENDAR_ROWS; i++) {
		for (j = 0; j < CALENDAR_COLUMS; j++) {
			if (child = gtk_grid_get_child_at(grid, j, i), child != NULL)
				gtk_widget_destroy(child);
		}
	}

	for (qtdDays = 1, i = 1, k = 1; i < CALENDAR_ROWS; i++) {
		for (j = 0; j < CALENDAR_COLUMS; j++) {

			if (i == 1 && j < weekFirstDay)	{ // dias do mes anterior
				box = create_day_box(calendar, 
						CAL_MONTH_DAYS[previous_mon] - weekFirstDay + j + 1,
						INACTIVE_DAY_FORMAT, TRUE);
			}
			else if (qtdDays <= CAL_MONTH_DAYS[calendar->mes]) { // dias do mes atual
				box = create_day_box(calendar, qtdDays, 
						(qtdDays == todayMDay) ? TODAY_FORMAT : DAY_FORMAT, FALSE);
				qtdDays++;
			}
			else
				box = create_day_box(calendar, k++, INACTIVE_DAY_FORMAT, TRUE); // dias do proximo mes

			gtk_grid_attach(grid, box, j, i, 1, 1);
		}
	}

	gtk_widget_show_all(GTK_WIDGET(grid));
}

GtkComboBoxText* inserir_meses_combo_box(GtkComboBoxText* comboBox) {
	gtk_combo_box_text_append_text(comboBox, "Janeiro"); 
	gtk_combo_box_text_append_text(comboBox, "Fevereiro"); 
	gtk_combo_box_text_append_text(comboBox, "Março"); 
	gtk_combo_box_text_append_text(comboBox, "Abril"); 
	gtk_combo_box_text_append_text(comboBox, "Maio"); 
	gtk_combo_box_text_append_text(comboBox, "Junho"); 
	gtk_combo_box_text_append_text(comboBox, "Julho"); 
	gtk_combo_box_text_append_text(comboBox, "Agosto"); 
	gtk_combo_box_text_append_text(comboBox, "Setembro"); 
	gtk_combo_box_text_append_text(comboBox, "Outubro"); 
	gtk_combo_box_text_append_text(comboBox, "Novembro"); 
	gtk_combo_box_text_append_text(comboBox, "Dezembro"); 
	return comboBox;
}

Calendario* novoCalendario(int mes, int ano) {
	Calendario* c = (Calendario*) g_malloc(sizeof(Calendario));
	if (!c) exit(1);
	c->mes = mes;
	c->ano = ano;

	return c;
}

Calendario* novoCalendarioHoje() {
	time_t now = time(NULL);
	struct tm *now_tm = localtime(&now);
	return novoCalendario(now_tm->tm_mon, now_tm->tm_year + 1900);
}

/* Criar um struct tm (time.h) a partir
 * de uma data passada nos parametros
 * day [1 - 31]
 * mon [0 - 11]
 * year [1 - INF] */
struct tm* convert_date_to_tm(int day, int mon, int year) {
	struct tm *day_tm = (struct tm*) malloc(sizeof(struct tm));

	if (!day_tm) exit(1);

	day_tm->tm_mday = day;
	day_tm->tm_mon = mon;
	day_tm->tm_year = year - 1900;
	day_tm->tm_sec = 0;
	day_tm->tm_min = 0;
	day_tm->tm_hour = 0;
	day_tm->tm_isdst = -1; // informacao nao conhecida

	return day_tm;
}

struct tm* copy_tm(struct tm* t) {
	struct tm *c = (struct tm*) malloc(sizeof(struct tm));
	if (!c) exit(1);

	c->tm_sec = t->tm_sec;
	c->tm_min = t->tm_min;
	c->tm_hour = t->tm_hour;
	c->tm_mday = t->tm_mday;
	c->tm_mon = t->tm_mon;
	c->tm_year = t->tm_year;
	c->tm_wday = t->tm_wday;
	c->tm_yday = t->tm_yday;
	c->tm_isdst = t->tm_isdst;

	return c;
}
