#ifndef ATIVIDADE_H
#define ATIVIDADE_H

#include <time.h>
#include <libnotify/notify.h>

#define ATIVIDADE_FILE ("atividades.bin")
#define ATIVIDADE_FREQUENCIA ("frequencia.txt")
#define COMEMORATIVE_DATES_FILE ("datas_comemorativas.txt")
#define COMEMORATIVE_DATES_FILE_FREQUENCIA ("datas_comemorativas_frequencia.txt")
#define DESC_TAM (50)

typedef struct {
	time_t inicio, fim;
	char desc[DESC_TAM];
} Atividade;

typedef struct {
	int dia, mes;
	char img[DESC_TAM];
	char nome[DESC_TAM];
	char hist[DESC_TAM];
} comemorative_date;

struct tm diferenca, data_de_hoje;

comemorative_date *date;

Atividade* novaAtividade(time_t inicio, time_t fim, const char* desc);
void salvarAtividade(Atividade* atividade);
Atividade* lerAtividades(int* tam);

int* lerFrequencias(int *tam_f);
int* nova_frequencia(int n,int *posicao);
void adicionar_frequencias(int tam,int posicao);
void atualizar_frequencias(int tam);

comemorative_date* nova_data_comemorativa(int dia, int mes, char* imagem, char* historia, char *name);
void adicionar_data_comemorativa(comemorative_date* cm_date);
comemorative_date* ler_datas_comemorativas(int *tam_c);

void adicionar_frequencias_date(int tam,int posicao);
int* nova_frequencia_date(int n,int *posicao);
int* ler_comemorative_date_frequencia(int *tam_f);
void atualizar_frequencias_date(int tam);
char text[128], dir[128];

NotifyNotification *aviso, *salvo;
int tam_f, tam_c;
int *frequencia, *date_frequencia;
int tempo;

#endif
